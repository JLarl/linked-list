
public class Node {
	Node next;
	int value;
	
	 public Node(int val)
	 {
		 this.value=val;
		 this.next=null;
	 }
	 
	 public void setNext(Node n)
	 {
		this.next=n; 
	 }
	 
	 public Node getNext()
	 {
		 return this.next;
	 }
	 
	 public boolean hasNext()
	 {
		 if(this.next==null)
		 {
			 return false;
		 }
		 return true;
	 }

	 public int getValue()
	 {
		 return this.value;
	 }
}
